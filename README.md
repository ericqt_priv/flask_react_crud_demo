Name: Qing Hui Tan

The first thing that should be done to get this going is to setup a python virtual environment:

I used anaconda as that is pretty easy to manage python virtual environments.

Next is to create a virtual environment for a python flask application - when you get anaconda running you can do something like

`conda install flask python-2.7` to specify the python version number for the flask virtual environment

next step is to activate your python environment and then type `flask run` and you should be all good to go for the python server.

For react part, I followed this guide to get started:
https://www.codecademy.com/articles/react-setup-i

After getting the necessary files take these steps to get the project running locally:
`python run.py -p  <5000>`
and then cd in to `./app` directory and type `npm run start`

send me a message at e.qing.tan@gmail.com if you have any questions.