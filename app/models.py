from app import db

class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), index=True)
    description = db.Column(db.String(120), index=True)
    done = db.Column(db.Boolean, index=True)

    def __init__(self, title, description, done):
    	self.title 			= title
    	self.description	= description
    	self.done			= done

    def update(self, title, description, done):
    	self.title 			= title
    	self.description 	= description
    	self.done 			= done
    
    def __str__(self):
    	return str({"id": self.id, "title": self.title, "description": self.description, "done": self.done})
    
    def __repr__(self):
        return '<Task title=%s, description=%s, done=%s>' % (self.title, self.description, self.done)

    def toDict(self):
    	return {"id": self.id, "title": self.title, "description": self.description, "done": self.done}