import React, {Component} from 'react';

class NewTask extends Component {
	constructor(props){
		super(props);
		this.setInput = this.setInput.bind(this);
		this.insertTask = this.insertTask.bind(this);
		this.clearFields = this.clearFields.bind(this);
		this.state = {
			title: "",
			description: "",
			done: 0
		}
	}

	insertTask(e){
		this.props.addTask(this.state);
	}

	setInput(e){
		let key = e.target.className;
		this.setState({ [key]: e.target.value})
	}

	clearFields(){
		this.setState({
			title: "",
			description: ""
		})
	}

	render(){
		return (
			<div>
				<div>
					<input className="title" maxLength="64" onChange={this.setInput} value={this.state.title} placeholder="Task Title"/>
				</div>
				<div>
					<textarea className="description" maxLength="120" onChange={this.setInput} value={this.state.description} placeholder="Task Description" />
				</div>
				<div>
					<button onClick={this.insertTask}>Save</button>
					<button onClick={this.clearFields}>Clear</button>
				</div>
			</div>
			);
	}
}

export default NewTask;