import React, {Component} from 'react';

class TaskItem extends Component{

	constructor(props){
		super(props);
		this.updateTask = this.updateTask.bind(this);
		this.toggleTaskDone = this.toggleTaskDone.bind(this);
		this.setInput = this.setInput.bind(this);
		this.deleteTask = this.deleteTask.bind(this);
		this.state = {
			title: this.props.title || '',
			description: this.props.description || '',
			id: this.props.id,
			done: this.props.done || false,
		}
	}

	updateTask(e, data_obj = this.state){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let init = {
			method: 'PATCH',
			headers: headers,
			cache: 'default',
			body: JSON.stringify(this.state),
		};
		let request = new Request('http://localhost:5000/tasks/'+this.state.id);
		let result = fetch(request, init).
		then(response => {return response.json()}).then((data) => {
			init.method = "GET";
			delete init.body;
			let singleTaskGet = new Request('http://localhost:5000/tasks/'+this.state.id);
			return fetch(singleTaskGet, init);
		}).
		then(response => {return response.json()}).then((data) => {
			this.setState({
				title: data.title,
				description: data.description,
				id: data.id,
				done: data.done,
			});
		});
	}

	deleteTask(e){
		this.props.deleteTask(this.state.id);
	}

	toggleTaskDone(e){
		let data_obj = this.state;
		data_obj.done = !data_obj.done;
		this.updateTask(e, data_obj);
	}

	setInput(e){
		let key = e.target.className;
		this.setState({ [key]: e.target.value});
	}

	render(){
			let doneStyle = this.state.done ? {"textDecorationLine":"line-through","textDecorationStyle":"solid"} : {"":""};

		return(
				<div style={{padding:10+'px'}}>
					<div>
						<input className="title" style={doneStyle} onChange={this.setInput} value={this.state.title} />
					</div>
					<div>
						<textarea className="description" style={doneStyle} onChange={this.setInput} value={this.state.description} />
					</div>
					<div>
						<button onClick={this.updateTask}>Update</button>
						<button onClick={this.deleteTask}>Delete</button>
						<button onClick={this.toggleTaskDone}>{this.state.done ? 'ToDo' : 'Done'}</button>
					</div>
				</div>
			)
	}
}

export default TaskItem;