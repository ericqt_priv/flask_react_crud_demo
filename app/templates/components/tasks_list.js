import React, {Component} from 'react';
import TaskItem from './task_item';


class TasksList extends Component {
	constructor(props) {
		super(props);
		this.deleteTask = this.deleteTask.bind(this);
		if(this.props.tasks_list.length == 0){
			console.log('still loading data')
		};
	}

	deleteTask(task_id){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let init = {
			method: 'DELETE',
			headers: headers,
			cache: 'default',
		};
		let request = new Request('http://localhost:5000/tasks/'+task_id);
		fetch(request, init).then((response) => {console.log(response); return response.json()}).
		then((data) => {
			this.props.fetchTasks();
		});
	}

	render(){
		let task_objs = this.props.tasks_list.map( (task_item) => {
			return(
				<TaskItem 
					title={task_item.title}
					description={task_item.description}
					id={task_item.id}
					done={task_item.done}
					key={task_item.id}
					deleteTask={this.deleteTask}
				/>
			);
		});
		return (
			<div>
				{task_objs}
			</div>
			);
	}
}

export default TasksList;