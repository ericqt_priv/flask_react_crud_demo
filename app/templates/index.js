'use strict';
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TasksList from './components/tasks_list';
import NewTask from './components/new_task';

class App extends Component {
	constructor(props){
		super(props)
		this.fetchTasks = this.fetchTasks.bind(this);
		this.addTask = this.addTask.bind(this);
		this.state = {
			tasks: []
		}
		this.fetchTasks();
	}

	fetchTasks(){
		//prepare the necessary params for requests
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let init = {
			method: 'GET',
			headers: Headers,
			cache: 'default',
		};
		let request = new Request('http://localhost:5000/tasks');
		fetch(request, init).then((response) => {return response.json()}).then((data)=> {
			this.setState({
				tasks: data
			});
		});
	}

	addTask(obj){
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let init = {
			method: 'POST',
			headers: headers,
			cache: 'default',
			body: JSON.stringify(obj),
		};
		let request = new Request('http://localhost:5000/tasks');
		fetch(request, init).then(response => {return response.json()}).then((data) => {
			this.fetchTasks();
		});
	}

	render(){
		return (
			<div>
				<NewTask 
					addTask={this.addTask}
					fetchTasks={this.fetchTasks}
				/>
				<TasksList 
					fetchTasks={this.fetchTasks}
					tasks_list={this.state.tasks} 
				/>
			</div>
			)
	}
}

ReactDOM.render(<App />, document.querySelector('.container'));