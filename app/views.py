from flask import render_template, request
from app import app, db
from models import Task
import json
from IPython import embed

@app.route('/')
@app.route('/index')

def index():
    return render_template('index.html',
                           title='Home')
                           
@app.route('/tasks', methods=['GET'])
def tasks():
    tasks = Task.query.order_by(Task.id.desc()).all()
    result = []
    for task in tasks:
        result.append(task.toDict())
    return json.dumps(result)
    
@app.route('/tasks/<id>')
def task(id):
    task = Task.query.get(int(id))
    if task == None:
        return "0"
    return json.dumps(task.toDict())

@app.route('/tasks', methods=['POST'])
def addTask():
    post_data = request.get_json()
    (done, description, title) = (post_data.get('done'), post_data.get('description'), post_data.get('title'))
    new_task = Task(title=title, description=description, done=done)
    db.session.add(new_task)
    db.session.commit()
    return str(new_task.id)

@app.route('/tasks/<id>', methods=['PATCH'])
def updateTask(id):
    task_id = int(id)
    data = request.get_json()
    task_to_update = Task.query.get(task_id)
    (title, description, done) = (data.get('title'), data.get('description'), data.get('done'))
    task_to_update.update(title, description, done)
    db.session.add(task_to_update)
    db.session.commit()
    return str(task_to_update.id)

@app.route('/tasks/<id>', methods=['DELETE'])
def deleteTask(id):
    task_id = int(id)
    task_to_delete = Task.query.get(task_id)
    db.session.delete(task_to_delete)
    db.session.commit()
    return str(task_to_delete.id)