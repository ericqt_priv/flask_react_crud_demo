var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
	template: __dirname + '/templates/index.html',
	filename: 'index.html'
})
module.exports = {
	entry: __dirname + '/templates/index.js',
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['react']
				}
			}
		]
	},
	output: {
		filename: 'transformed.js',
		path: __dirname + '/build'
	},
	devServer: {
		historyApiFallback: true,
		contentBase: './'
	},
	plugins: [HTMLWebpackPluginConfig]
};